class StudentEvaluationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_course, only: [:index, :statistics]
  before_action :set_student, only: [:update]

  def index
    @students    = Student.where course: @course
    @evaluations = Evaluation.where course: @course
  end

  def statistics
    @students    = Student.where course: @course
    @evaluations = Evaluation.where course: @course
  end
  
  def update
    params[:scores].each do |evaluation_id, score|
      evaluation = Evaluation.find(evaluation_id)
      # if it's blank count as absent
      if score.blank? 
        delete_student_evaluation(evaluation)
      else
        student_evaluation = find_or_create_student_evaluation(evaluation)
        student_evaluation.score = score
        if not student_evaluation.valid?
          render json: student_evaluation.errors, status: :unprocessable_entity and return
        end

        student_evaluation.save!
        if not student_evaluation.save
          render json: student_evaluation.errors, status: :unprocessable_entity and return
        end
      end
    end
    render json: { "response" => :success }
  end

  private
    def set_course
      @course = Course.find(params[:course_id])
    end

    def set_student
      @student = Student.find(params[:student_id])
    end

    def find_or_create_student_evaluation(evaluation) 
      # search the evaluation
      student_evaluation = @student.find_evaluation(evaluation)
      if not student_evaluation
        # create a new evaluation
        student_evaluation = StudentEvaluation.new
        student_evaluation.student = @student
        student_evaluation.evaluation = evaluation
      end
      return student_evaluation
    end

    def delete_student_evaluation(evaluation)
      student_evaluation = @student.find_evaluation(evaluation)
      # if existed before
      if student_evaluation
        @student.delete_student_evaluation(student_evaluation)
        @student.save!
      end
    end

end
