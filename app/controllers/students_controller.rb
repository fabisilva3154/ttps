class StudentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_student, only: [:edit, :update, :destroy]
  before_action :set_course,  only: [:new, :create, :edit, :destroy, :update]

  # GET /students
  # GET /students.json
  # def index
  #   @students = Student.where(course: @course)
  # end

  # GET /students/1
  # GET /students/1.json
  # def show
  # end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)
    @student.course = @course

    respond_to do |format|
      if @student.save
        format.html { redirect_to course_path(@course), notice: t('controllers.student.created') }
        format.json { render :show, status: :created, location: course_student_path(@course) }
      else
        puts @student.errors.full_messages
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to course_path(@course), notice: t('controllers.student.updated') }
        format.json { render :show, status: :ok, location: course_student_path(@student.course, @student) }
      else
        puts @student.errors.full_messages
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to course_path(@course), notice: t('controllers.student.destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    def set_course
      @course = Course.find(params[:course_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:first_name, :last_name, :dni, :file_number, :email, :course_id)
    end
end
