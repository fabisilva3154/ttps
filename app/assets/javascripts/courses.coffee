# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  $('.tabular.menu .item').tab();
  $('.tabular.menu .item').tab('change tab', 'tab-info');

  if $('.ui.positive.message').length
    setTimeout ( -> 
        $('.ui.positive.message').empty();
        $('.ui.positive.message').slideUp('fast');
    ), 3000