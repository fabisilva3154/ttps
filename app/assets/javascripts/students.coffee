# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "turbolinks:load", ->

  alertify.set('notifier','position', 'top-right');

  # habilita el boton de guardar al escribir en el input  
  $(".score-table .score").keydown (e)->
    score = $(e.currentTarget)
    row = $('#student-' + score.data('student'))
    row.find('.save-score').removeClass('disabled')

  # guarda las notas del alumno
  $(".score-table .save-score").click (e)->
    save_button = $(e.currentTarget)
    row = $('#student-' + save_button.data('student'))
    save(row)

  # obtiene las notas del alumno y las guarda
  save = (row)->
    evaluations_inputs = row.find('.score')
    evaluations_scores = {}
    scores = $.each(evaluations_inputs, (idx, score)-> 
      evaluation_id = $(score).data('evaluation')
      evaluations_scores[evaluation_id] = $(score).val().trim()
    );
    if !scores_are_valid(evaluations_scores)
      return false
    course_id  = $(evaluations_inputs).data('course')
    student_id = $(evaluations_inputs).data('student')
    row.find('.save-score').addClass('disabled')
    row.find('.save-score').text('Guardando...')
    $.ajax({
      type : 'PUT',
      url  : '/courses/'+ course_id + '/student_evaluations/' + student_id
      data : 
        scores : evaluations_scores
      dataType : 'json',
      success : (response) ->
        row.find('.save-score').text('Guardar')
        alertify.success('Notas guardadas correctamente');
      error : (response) ->
        row.find('.save-score').text('Guardar')
        $.each(response.responseJSON, (attribute, error_message) -> 
          alertify.error("#{attribute} #{error_message}");
        );
    })

  scores_are_valid = (evaluations_scores)->
    is_valid = true
    $.each(evaluations_scores, (evaluation_id, score)->
      if !is_positive_integer(score)
        alertify.error("\"#{score}\" no es una nota válida")
        is_valid = false
    );
    return is_valid

  is_positive_integer = (str)-> 
    return Number(str) >= 0
  