class Evaluation < ApplicationRecord
  belongs_to :course
  has_many :student_evaluations, dependent: :destroy
  validates :name, presence: true
  validates :date, presence: true
  validates :passing_score, numericality: { greater_than_or_equal_to: 0 }
end
