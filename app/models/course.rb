class Course < ApplicationRecord
  has_many :students, dependent: :destroy
  has_many :evaluations, dependent: :destroy
  validates :name, presence: true
  validates :year, presence: true, numericality: { only_integer: true }

  def passing_students(evaluation)
    Student.passing_students(evaluation)
  end

  def failed_students(evaluation)
    Student.failed_students(evaluation)
  end

  def absent_students(evaluation)
    Student.absent_students(evaluation)
  end

  def passing_percentage(evaluation)
    passing_students = passing_students(evaluation)
    failed_students  = failed_students(evaluation)
    if passing_students.length + failed_students.length > 0
      return (passing_students.length * 100) / (passing_students.length + failed_students.length)
    end
    0
  end
end
