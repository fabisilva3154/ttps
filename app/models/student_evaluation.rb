class StudentEvaluation < ApplicationRecord
  belongs_to :evaluation
  belongs_to :student
  validates :score, numericality: { greater_than_or_equal_to: 0 }
  validates :evaluation_id, uniqueness: { scope: :student_id, message: I18n.translate('activerecord.errors.messages.not_unique_evaluation') }
  validate :same_course

  def same_course
    if evaluation.course != student.course
      errors.add(:not_same_course, I18n.translate('activerecord.errors.messages.not_same_course'))
    end
  end

end
