class Student < ApplicationRecord
  belongs_to :course
  has_many :student_evaluations, dependent: :destroy
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :dni, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :email, presence: true, email: true
  validates :file_number, presence: true
  scope :present_students, -> (evaluation) { joins(:student_evaluations).where("evaluation_id =  ?", evaluation.id) }
  scope :passing_students, -> (evaluation) { Student.present_students(evaluation).joins("INNER JOIN evaluations ON student_evaluations.evaluation_id = evaluations.id").where("score >= passing_score") }
  scope :failed_students,  -> (evaluation) { Student.present_students(evaluation).joins("INNER JOIN evaluations ON student_evaluations.evaluation_id = evaluations.id").where("score <  passing_score") }
  scope :absent_students,  -> (evaluation) { where(course_id: evaluation.course_id).where.not(id: Student.present_students(evaluation).pluck(:id)) }

  def find_evaluation(evaluation)
    StudentEvaluation.find_by(evaluation_id: evaluation.id, student_id: id)
  end

  def delete_student_evaluation(student_evaluation)
    self.student_evaluations.delete(student_evaluation)
  end

  def score_value(evaluation)
    if not absent?(evaluation)
      find_evaluation(evaluation).score
    end
  end
  
  def passed?(evaluation)
    not absent?(evaluation) and (score_value(evaluation) >= evaluation.passing_score)
  end

  def failed?(evaluation)
    not absent?(evaluation) and (score_value(evaluation) < evaluation.passing_score)
  end
  
  def absent?(evaluation)
    not find_evaluation(evaluation)
  end

end
