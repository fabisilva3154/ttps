json.extract! student, :id, :first_name, :last_name, :dni, :email, :file_number, :course_id, :created_at, :updated_at
json.url student_url(student, format: :json)
