json.extract! evaluation, :id, :name, :passing_score, :date, :course_id, :created_at, :updated_at
json.url evaluation_url(evaluation, format: :json)
