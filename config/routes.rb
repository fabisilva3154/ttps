Rails.application.routes.draw do
    
  devise_for :users
  resources :courses do
    resources :students,    :except => [:index, :show]
    resources :evaluations, :except => [:index, :show]
    
    get 'student_evaluations',             to: 'student_evaluations#index'
    put 'student_evaluations/:student_id', to: 'student_evaluations#update'
    get 'student_statistics',              to: 'student_evaluations#statistics'

  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  
  # root for devise
  # root to: 'admin#index'
  root to: 'courses#index'


end
