class CreateStudentEvaluations < ActiveRecord::Migration[5.1]
  def change
    create_table :student_evaluations do |t|
      t.float :score
      t.references :evaluation, foreign_key: true
      t.references :student, foreign_key: true

      t.timestamps
    end
  end
end
