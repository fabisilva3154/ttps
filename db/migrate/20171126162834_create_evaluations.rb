class CreateEvaluations < ActiveRecord::Migration[5.1]
  def change
    create_table :evaluations do |t|
      t.string :name
      t.float :passing_score
      t.date :date
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
