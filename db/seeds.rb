# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
admin = User.new({ first_name: 'Pablo', last_name: 'Silva', email: 'usuario@mail.com', password: '123456', password_confirmation: '123456' })
admin.save!


course_2016 = Course.create({ name: 'Cursada', year: 2016 })
course_2017 = Course.create({ name: 'Cursada', year: 2017 })
char_base = 'A'.ord - 1
# Course 2016
# Students
(1..20).each do |index| 
  Student.create({ 
    first_name: 'Alumno', 
    last_name: (char_base + index).chr,
    dni: 3000000 + (index * 10000), 
    email: "alumno#{index}@mail.com",
    file_number: "1#{index}000/1",
    course: course_2016
  })
end
# Evaluations
(1..3).each do |index| 
  evaluation = Evaluation.create({ 
    name: "Parcial #{index}", 
    passing_score: 7.0,
    date: Time.new(2016, 12, index),
    course: course_2016
  })
end
students_course_2016 = Student.where({ course_id: course_2016.id })
evaluations_course_2016 = Evaluation.where({ course_id: course_2016.id })
students_course_2016.sample(10).each do |student|
  evaluations_course_2016.each do |evaluation|
    StudentEvaluation.create({ student: student, evaluation: evaluation, score: rand(10) })
  end
end


# Course 2017
# Students
(1..20).each do |index| 
  Student.create({ 
    first_name: 'Alumno', 
    last_name: (char_base + index).chr,
    dni: 3000000 + (index * 10000), 
    email: "alumno#{index}@mail.com",
    file_number: "1#{index}000/1",
    course: course_2017
  })
end
# Evaluations
students_course_2017 = Student.where({ course_id: course_2017.id })
(1..3).each do |index| 
  evaluation = Evaluation.create({ 
    name: "Parcial #{index}", 
    passing_score: 7.0, 
    date: Time.new(2017, 12, index),
    course: course_2017
  })
end
students_course_2017 = Student.where({ course_id: course_2017.id })
evaluations_course_2017 = Evaluation.where({ course_id: course_2017.id })
students_course_2017.sample(10).each do |student|
  evaluations_course_2017.each do |evaluation|
    StudentEvaluation.create({ student: student, evaluation: evaluation, score: rand(10) })
  end
end
