require 'test_helper'

class EvaluationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @evaluation = evaluations(:one)
    sign_in users(:one)
  end

  test "should get new" do
    get new_course_evaluation_url(@evaluation.course)
    assert_response :success
  end

  test "should create evaluation" do
    assert_difference('Evaluation.count') do
      post course_evaluations_url(@evaluation.course), params: { evaluation: { course_id: @evaluation.course_id, date: @evaluation.date, name: @evaluation.name, passing_score: @evaluation.passing_score } }
    end

    assert_redirected_to course_url(@evaluation.course)
  end

  test "should get edit" do
    get edit_course_evaluation_url(@evaluation.course, @evaluation)
    assert_response :success
  end

  test "should update evaluation" do
    patch course_evaluation_url(@evaluation.course, @evaluation), params: { evaluation: { course_id: @evaluation.course_id, date: @evaluation.date, name: @evaluation.name, passing_score: @evaluation.passing_score } }
    assert_redirected_to course_url(@evaluation.course)
  end

  test "should destroy evaluation" do
    assert_difference('Evaluation.count', -1) do
      delete course_evaluation_url(@evaluation.course, @evaluation)
    end

    assert_redirected_to course_url(@evaluation.course)
  end
end