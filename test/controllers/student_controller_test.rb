require 'test_helper'

class StudentsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @student = students(:one)
    sign_in users(:one)
  end

  test "should get new" do
    get new_course_student_url(@student.course)
    assert_response :success
  end

  test "should create student" do
    assert_difference('Student.count') do
      post course_students_url(@student.course), params: { student: { course_id: @student.course_id, dni: @student.dni, email: @student.email, file_number: @student.file_number, first_name: @student.first_name, last_name: @student.last_name } }
    end

    assert_redirected_to course_url(@student.course)
  end

  test "should get edit" do
    get edit_course_student_url(@student.course, @student)
    assert_response :success
  end

  test "should update student" do
    patch course_student_url(@student.course, @student), params: { student: { course_id: @student.course_id, dni: @student.dni, file_number: @student.file_number, first_name: @student.first_name, last_name: @student.last_name, email: @student.email } }
    assert_redirected_to course_url(@student.course)
  end

  test "should destroy student" do
    assert_difference('Student.count', -1) do
      delete course_student_url(@student.course, @student)
    end

    assert_redirected_to course_url(@student.course)
  end
end