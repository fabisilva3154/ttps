require 'test_helper'

class EvaluationTest < ActiveSupport::TestCase

  def setup
    @course     = Course.create({ year: 2015, name: 'Course' })
    @evaluation = Evaluation.create({ name: 'Eval 1', passing_score: 5, date: Time.new, course: @course })
  end

  test "should not save an empty evaluation" do
    evaluation = Evaluation.new
    assert_not evaluation.save
  end

  test "should not save evaluation with an invalid date" do
    @evaluation.date = 'string'
    assert_not @evaluation.save
  end

  test "should not save evaluation with an invalid passing score" do
    @evaluation.passing_score = 'string'
    assert_not @evaluation.save
  end

  test "should save evaluation" do
    assert @evaluation.save
  end
end
