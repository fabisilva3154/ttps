require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  
  def setup
    @course     = Course.create({ year: 2015, name: 'Course' })
    @student1   = Student.create({ first_name: 'A', last_name: 'A', dni: 0, file_number: '1', email: 'a@a.com', course: @course })
    @student2   = Student.create({ first_name: 'B', last_name: 'B', dni: 0, file_number: '1', email: 'a@a.com', course: @course })
    @student3   = Student.create({ first_name: 'C', last_name: 'C', dni: 0, file_number: '1', email: 'a@a.com', course: @course })
    @student4   = Student.create({ first_name: 'D', last_name: 'D', dni: 0, file_number: '1', email: 'a@a.com', course: @course })
    @student5   = Student.create({ first_name: 'E', last_name: 'E', dni: 0, file_number: '1', email: 'a@a.com', course: @course })
    @evaluation1 = Evaluation.create({ name: 'Eval 1', passing_score: 10, date: Time.new, course: @course })
    @evaluation2 = Evaluation.create({ name: 'Eval 2', passing_score: 10, date: Time.new, course: @course })
    @evaluation3 = Evaluation.create({ name: 'Eval 3', passing_score: 10, date: Time.new, course: @course })
    @evaluation4 = Evaluation.create({ name: 'Eval 4', passing_score: 10, date: Time.new, course: @course })
    @evaluation5 = Evaluation.create({ name: 'Eval 5', passing_score: 10, date: Time.new, course: @course })
    @course.save!
  end

  test "should not save course without name or year" do
    course = Course.new
    assert_not course.save
  end

  test "should not save course with an invalid year" do
    course = Course.new
    course.name = "Name"
    course.year = 'string'
    assert_not course.save
  end

  test "should save course" do
    course = Course.new
    course.year = 2015
    course.name = 'Course'
    assert course.save
  end

  test "should approved student be in approved students list" do
    assert_not @course.passing_students(@evaluation1).include?(@student1)
    @student1.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    @student2.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    @student3.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    @student4.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    @student5.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    assert @course.passing_students(@evaluation1).include?(@student1)
  end

  test "should calculate the totals of approved students" do
    assert_equal(0, @course.passing_students(@evaluation1).length)

    @student1.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    @student2.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    @student3.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    @student4.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    @student5.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    assert_equal(5, @course.passing_students(@evaluation1).length)

    @student1.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    @student2.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    @student3.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    @student4.student_evaluations.create({ evaluation: @evaluation2, score: 10 })
    @student5.student_evaluations.create({ evaluation: @evaluation2, score: 10 })
    assert_equal(2, @course.passing_students(@evaluation2).length)
  end

  test "should calculate the totals of failed students" do
    assert_equal(0, @course.failed_students(@evaluation1).length)

    @student1.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    @student2.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    @student3.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    @student4.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    @student5.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    assert_equal(5, @course.failed_students(@evaluation1).length)

    @student1.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    @student2.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    @student3.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    @student4.student_evaluations.create({ evaluation: @evaluation2, score: 10 })
    @student5.student_evaluations.create({ evaluation: @evaluation2, score: 10 })
    assert_equal(3, @course.failed_students(@evaluation2).length)
  end

  test "should calculate the totals of absent students" do
    assert_equal(5, @course.absent_students(@evaluation1).length)

    @student1.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    @student2.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    @student3.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    @student4.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    @student5.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    assert_equal(0, @course.absent_students(@evaluation1).length)

    @student1.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    @student2.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    @student3.student_evaluations.create({ evaluation: @evaluation2, score: 5 })
    assert_equal(2, @course.absent_students(@evaluation2).length)
  end

  test "should calculate the passing percentage correctly" do
    student_evaluation = @student1.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    assert_equal(100, @course.passing_percentage(@evaluation1))

    student_evaluation = @student2.student_evaluations.create({ evaluation: @evaluation1, score: 5 })
    assert_equal(50, @course.passing_percentage(@evaluation1))
  end

end
