require 'test_helper'

class StudentEvaluationTest < ActiveSupport::TestCase

  def setup
    @course        = Course.create({ year: 2015, name: 'Course' })
    @student1      = Student.create({ first_name: 'A', last_name: 'A', dni: 0, file_number: '1', email: 'a@a.com', course: @course })
    @evaluation1   = Evaluation.create({ name: 'Eval 1', passing_score: 10, date: Time.new, course: @course })
    @other_course  = Course.create({ year: 2015, name: 'Course' })
    @student_from_other_course = Student.create({ first_name: 'A', last_name: 'A', dni: 0, file_number: '1', email: 'a@a.com', course: @other_course })
    @course.save!
  end

  test "student evaluation should be invalid" do
    student_evaluation = student_evaluations(:one)
    student_evaluation.score = -1
    assert_not student_evaluation.save
  end

  test "evaluation course and student course must be the same" do
    student_evaluation = StudentEvaluation.create(evaluation: @evaluation1, student: @student_from_other_course, score: 10)
    assert_not_nil student_evaluation.errors[:not_same_course]
  end

  test "student should pass the evaluation" do
    student_evaluation = student_evaluations(:passing)
    student = student_evaluation.student
    evaluation = student_evaluation.evaluation
    assert student.passed?(evaluation)
  end

  test "student should failed the evaluation" do
    student_evaluation = student_evaluations(:failed)
    student = student_evaluation.student
    evaluation = student_evaluation.evaluation
    assert student.failed?(evaluation)
  end

  test "student should have been absent for the evaluation" do
    student = students(:three)
    evaluation = evaluations(:one)
    assert student.absent?(evaluation)
  end

  test "should not save a repeated evaluation from the same student" do
    student_evaluation = student_evaluations(:one)
    student_evaluation.save

    student_evaluation = student_evaluations(:repeated_one)
    student_evaluation.save

    assert_not student_evaluation.save
  end

  test "should get the same score value" do
    @student1.student_evaluations.create({ evaluation: @evaluation1, score: 10 })
    assert_equal(10, @student1.score_value(@evaluation1))
  end

end
