require 'test_helper'

class StudentTest < ActiveSupport::TestCase
  def setup
    @course  = Course.create({ year: 2015, name: 'Course' })
    @student = Student.create({ first_name: 'A', last_name: 'A', dni: 0, file_number: '1', email: 'a@a.com', course: @course })
  end

  test "should not save an empty student" do
    student = Student.new
    assert_not student.save
  end

  test "should not save student with invalid DNI" do
    @student.dni = -1
    assert_not @student.save
  end

end
