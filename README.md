TTPS 2017 - Trabajo Final Integrador
===================
## Pablo Silva ##

----------
Links
-------------
**Bitbucket:** https://bitbucket.org/fabisilva3154/ttps.git

**Heroku:** https://damp-plains-43981.herokuapp.com


Tecnologías usadas
-------------

* SQLite3
* Devise
* Semantic UI
* Alertify.js

Ejecutar la aplicación
-------------

* bundle install
* rake db:migrate
* rake db:seed
* rails server
* Ir a http://localhost:3000 y loguearse con:
  - Email: **usuario@mail.com**
  - Contraseña: **123456**
   

Ejecutar los tests
-------------

* rails db:migrate RAILS_ENV=test
* rails test test/models
* rails test test/controllers